<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form(){
        return view ("form");
    }

    public function welcome(Request $request){
        return view ("welcome");
    }

    public function welcome_post(Request $request){
        $namadepan = $request->input("Firstname");
        $namabelakang = $request->input("Lastname");
        return view ("welcome",["namadepan"=> $namadepan,"namabelakang" => $namabelakang]);
    }
}
