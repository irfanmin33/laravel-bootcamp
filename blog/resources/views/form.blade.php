<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="Firstname"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="Lastname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>
        <label>Nationality:</label><br><br>
        <select name="Nation">
            <option value="Indonesian">Indonesian</option>
            <option value="Singaporean">Singaporean</option>
            <option value="Malaysian">Malaysian</option>
        </select> <br><br>
        <label>Languange Spoken:</label><br><br>
        <input type="checkbox"> Bahasa Indonesia
        <input type="checkbox"> English
        <input type="checkbox"> Other <br><br>
        <label>Bio</label><br><br>
        <textarea name="bio" cols="30" rows="7"></textarea><br>
        <input type="submit" >
    </form>

</body>
</html>