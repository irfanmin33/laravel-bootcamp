@extends('master')

@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Buat Data Pemain Baru</h3>
              </div>
              <form role="form" action="/cast" method="POST">
              <!-- /.card-header -->
              <!-- form start -->
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name='nama' value="{{old('nama','' )}}" placeholder="Nama">
                  </div>
                  @error('nama')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="number" class="form-control" id="umur" name='umur' value="{{old('umur','' )}}" placeholder="Umur">
                  </div>
                  @error('umur')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="form-group">
                    <label for="bio">Bio</label>
                    <input type="text" class="form-control" id="bio" name='bio' value="{{old('bio','' )}}" placeholder="Bio">
                  </div>
                  @error('bio')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
    
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>

@endsection