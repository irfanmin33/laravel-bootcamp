@extends('master')

@section('content')
<div class="card">
              <div class="card-header">
                <h3 class="card-title">Cast</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <a class ="btn btn-primary mb-3" href="/cast/create"> Create New Cast </a>
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th style="width: 300px">Nama</th>
                      <th style="width: 30px">Umur</th>
                      <th>Bio</th>
                      <th style="width: 40px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                   @forelse ($cast as $key => $cast)
                    <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $cast -> nama }} </td>
                        <td> {{ $cast -> umur }} </td>
                        <td> {{ $cast ->bio }} </td>
                        <td style="display :flex;"> 
                            <a class="btn btn-info" href="/cast/{{ $cast->id}}"> Show </a>
                            <a class="btn btn-default" href="/cast/{{ $cast->id }}/edit"> Edit </a> 
                            <form action="/cast/{{ $cast->id }}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center"> No Cast </td>
                    </tr?
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>
@endsection