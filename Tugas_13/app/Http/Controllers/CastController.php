<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create() {
        return view('cast.create');
    }

    public function store(request $request) {
        // dd($request->all());
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'bio'=>'required',
        ]);
        $query = DB::table('cast')->insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);
        return redirect('/cast'); 
    }

    public function index() {
        $cast = DB::table('cast')->get();
        return view ('cast.index',compact('cast'));
    }

    public function show($id) {
        $cast = DB::table('cast')->where('id',$id)->first();
        // dd($cast);
        return view ('cast.show',compact('cast'));
    }

    public function edit($id) {
        $cast = DB::table('cast')->where('id',$id)->first();
        // dd($cast);
        return view ('cast.edit',compact('cast'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'bio'=>'required',
        ]);

        $cast = DB::table('cast')->where('id',$id)->
        update([
            'nama'=>$request['nama'],
            'umur'=>$request['umur'],
            'bio'=>$request['bio'],
        ]);
        // dd($cast);
        return redirect ('/cast');
    }

    public function destroy($id) {
        $cast = DB::table('cast')->where('id',$id)->delete();
        // dd($cast);
        return redirect ('/cast');
    }
}

